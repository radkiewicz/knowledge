package pl.poznan.put.fc.knowledgebase.controller;

import com.microsoft.azure.storage.StorageException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.xmlbeans.XmlException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.poznan.put.fc.knowledgebase.filerepository.AzureRepositoryManager;
import pl.poznan.put.fc.knowledgebase.model.Document;
import pl.poznan.put.fc.knowledgebase.service.DocumentService;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.List;

@RestController
@RequestMapping(value = "document")
public class DocumentController {

    @Autowired
    DocumentService documentService;

    @Autowired
    AzureRepositoryManager azureRepositoryManager;

    @GetMapping(value = "/{id}")
    public Document get(@PathVariable Long id) {
        return documentService.getDocument(id);
    }

    @GetMapping
    public List<Document> getAll() {
        return documentService.getAllDocuments();
    }

    @GetMapping(value = "reimport")
    public void reimportDocuments() throws OpenXML4JException, XmlException, IOException, InvalidKeyException, StorageException, URISyntaxException {
        documentService.reimportAllDocuments();
    }

    @GetMapping(value = "/search")
    public List<Document> search(@RequestParam(name = "query", required = true) String query) {
        return documentService.searchByContentMeaning(query);
    }

    @GetMapping(value = "/test")
    public List<Document> test() throws InvalidKeyException, IOException, URISyntaxException, StorageException {
        return azureRepositoryManager.getAllDocuments();
    }

}
