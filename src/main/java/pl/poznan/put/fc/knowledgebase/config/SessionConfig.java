package pl.poznan.put.fc.knowledgebase.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import pl.poznan.put.fc.knowledgebase.model.Session;

@Configuration
public class SessionConfig {
    @Bean
    @Scope("session")
    public Session session(){
        return new Session(false);
    }
}
