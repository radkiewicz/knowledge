package pl.poznan.put.fc.knowledgebase;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.poznan.put.fc.knowledgebase.service.DocumentService;

@SpringBootApplication
@Slf4j
public class KnowledgeBaseApplication{

    @Autowired
    private DocumentService documentService;

    public static void main(String[] args) {
        SpringApplication.run(KnowledgeBaseApplication.class, args);
    }
}
