package pl.poznan.put.fc.knowledgebase.service;

import com.microsoft.azure.storage.StorageException;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.extractor.POITextExtractor;
import org.apache.poi.ooxml.extractor.ExtractorFactory;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.xmlbeans.XmlException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.poznan.put.fc.knowledgebase.filerepository.AzureRepositoryManager;
import pl.poznan.put.fc.knowledgebase.model.Document;
import pl.poznan.put.fc.knowledgebase.repository.DocumentRepository;
import pl.poznan.put.fc.knowledgebase.repository.DocumentSearchRepository;
import pl.poznan.put.fc.knowledgebase.utils.LevenshteinDistanceCalculator;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DocumentService {
    @Autowired
    private DocumentRepository documentRepository;
    @Autowired
    private AzureRepositoryManager azureRepositoryManager;

    @Autowired
    private DocumentSearchRepository documentSearchRepository;

    public Document getDocument(Long id) {
        return documentRepository.findById(id).get();
    }

    public List<Document> getAllDocuments() {
        return documentRepository.findAll();
    }

    public List<Document> searchByContentMeaning(String query) {
        return searchWithFreeTextTable(query);
    }

    public List<Document> searchByContentPhrase(String query) {
        return documentSearchRepository.containsSearch(query);
    }

    private List<Document> searchWithFreeTextTable(String query) {
        return documentSearchRepository.freeTextTableSearch(query);
    }

    private List<Document> searchByLevenshteinDistance(String query) {
        return documentRepository.findAll().stream()
                .filter(document -> {
                    int lowestDistance = 1000;
                    String[] queryWords = query.split(" ");
                    String[] content = document.getTextContent()
                            .replace("\n", "")
                            .replace("\r", "")
                            .replace("\t", "")
                            .split(" ");
                    for (int i = 0; i < content.length - queryWords.length + 1; i++) {
                        int calculate = LevenshteinDistanceCalculator.calculate(query, subContent(content, i, i + queryWords.length - 1));
                        if (calculate < 3) {
                            return true;
                        }
                    }
                    return false;
                }).collect(Collectors.toList());
    }

    private String subContent(String[] content, int startIndex, int endIndex) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i <= endIndex - startIndex; i++) {
            stringBuilder.append(content[startIndex + i]);
            stringBuilder.append(" ");
        }
        return stringBuilder.toString().trim();
    }

    public List<Document> searchByTags(String query) {
        return documentRepository.findAll().stream()
                .filter(document -> document.getTags().stream().anyMatch(tag -> tag.equalsIgnoreCase(query)))
                .collect(Collectors.toList());
    }

    public List<Document> searchByName(String name) {
        return documentRepository.findAll().stream()
                .filter(document -> document.getName().toUpperCase().contains(name.toUpperCase()))
                .collect(Collectors.toList());
    }

    public Document addTagsToDocument(Document document, List<String> newTags) {
        document.getTags().addAll(newTags);
        document.setTags(document.getTags().stream().distinct().collect(Collectors.toList()));
        return documentRepository.save(document);
    }

    public void reimportAllDocuments() throws IOException, OpenXML4JException, XmlException, InvalidKeyException, StorageException, URISyntaxException {
        List<Document> newDocuments = azureRepositoryManager.getAllDocuments();
        ListIterator<Document> documentListIterator = newDocuments.listIterator();
        List<Document> oldDocuments = documentRepository.findAll();
        List<Document> documentsToDelete = new ArrayList<>();
        oldDocuments.forEach(od -> {
            if (newDocuments.stream().filter(nd -> nd.getPath().equals(od.getPath())).findFirst().isEmpty()) {
                documentsToDelete.add(od);
            }
        });
        documentsToDelete.forEach(d -> documentRepository.delete(d));

        while (documentListIterator.hasNext()) {
            Document next = documentListIterator.next();
            List<Document> documents = documentRepository.findByPath(next.getPath());
            if (!documents.isEmpty()) {
                Document document = documents.get(0);
                document.setTextContent(readDocumentContent(next));
                documentRepository.save(document);
            } else {
                next.setTextContent(readDocumentContent(next));
                documentRepository.save(next);
            }
            documentListIterator.remove();
        }
    }

    public String readDocumentContent(Document document) throws IOException, OpenXML4JException, XmlException, InvalidKeyException, StorageException, URISyntaxException {
        String result = null;
        if (document.getExtension().toLowerCase().contains("pdf")) {
            result = readPdfContent(document);
        } else if (document.getExtension().toLowerCase().contains("docx") || document.getExtension().toLowerCase().contains("doc")) {
            result = readWordFileContent(document);
        } else if (document.getExtension().toLowerCase().contains("txt")) {
            result = readTxtFileContent(document);
        }
        return result;
    }

    public byte[] getPdfPreview(Document doc) throws IOException, StorageException, InvalidKeyException, URISyntaxException {
        Path path = null;
        try {
            path = prepareTempFile(doc);
            try (PDDocument document = PDDocument.load(path.toFile()); ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                BufferedImage resized = resizeToPreview(new PDFRenderer(document).renderImageWithDPI(0, 300, ImageType.RGB));
                ImageIO.write(resized, "jpg", baos);
                baos.flush();
                return baos.toByteArray();
            }
        } finally {
            Files.deleteIfExists(path);
        }
    }

    private static BufferedImage resizeToPreview(BufferedImage img) {
        int i = img.getWidth() / 1000;
        if (i < 1) {
            return img;
        } else {
            int newW, newH;
            newH = img.getHeight() / i;
            newW = img.getWidth() / i;
            Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_DEFAULT);
            BufferedImage dimg = new BufferedImage(newW, newH, img.getType());

            Graphics2D g2d = dimg.createGraphics();
            g2d.drawImage(tmp, 0, 0, null);
            g2d.dispose();

            return dimg;
        }
    }

    private String readPdfContent(Document document) throws IOException, StorageException, InvalidKeyException, URISyntaxException {
        Path path = null;
        try {
            path = prepareTempFile(document);
            String text = null;
            PDDocument doc = PDDocument.load(path.toFile());

            if (!doc.isEncrypted()) {
                PDFTextStripper stripper = new PDFTextStripper();
                text = stripper.getText(doc);
            }
            doc.close();
            return text;
        } finally {
            Files.deleteIfExists(path);
        }
    }

    private String readWordFileContent(Document document) throws IOException, OpenXML4JException, XmlException, StorageException, InvalidKeyException, URISyntaxException {
        Path path = null;
        try {
            path = prepareTempFile(document);
            try (POITextExtractor extractor = ExtractorFactory.createExtractor(path.toFile())) {
                return extractor.getText().replaceAll("\\t|\\r|\\n+", " ");
            }
        } finally {
            Files.deleteIfExists(path);
        }
    }

    private String readTxtFileContent(Document document) throws IOException, InvalidKeyException, StorageException, URISyntaxException {
        Path path = null;
        try {
            path = prepareTempFile(document);
            return Files.readString(path);
        } finally {
            Files.deleteIfExists(path);
        }
    }

    private Path prepareTempFile(Document document) throws URISyntaxException, StorageException, InvalidKeyException, IOException {
        try (InputStream is = azureRepositoryManager.getFile(document.getPath()).openRead()) {
            Path temp = Files.createFile(Paths.get("temp"));
            Files.write(temp, is.readAllBytes());
            return temp;
        }
    }

    public byte[] getDocumentFile(Document document) throws URISyntaxException, StorageException, InvalidKeyException, IOException {
        try (InputStream is = azureRepositoryManager.getFile(document.getPath()).openRead()) {
            return is.readAllBytes();
        }
    }
}
