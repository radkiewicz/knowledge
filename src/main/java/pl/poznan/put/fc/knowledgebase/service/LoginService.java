package pl.poznan.put.fc.knowledgebase.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.poznan.put.fc.knowledgebase.model.User;
import pl.poznan.put.fc.knowledgebase.repository.UsersRepository;

@Service
public class LoginService {
    @Autowired
    UsersRepository usersRepository;

    public boolean validateCredentials(String login, String password){
        User firstByLogin = usersRepository.findFirstByLogin(login);
        return firstByLogin.getPassword().equals(password);
    }
}
