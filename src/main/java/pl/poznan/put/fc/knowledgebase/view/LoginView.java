package pl.poznan.put.fc.knowledgebase.view;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import pl.poznan.put.fc.knowledgebase.model.Session;
import pl.poznan.put.fc.knowledgebase.service.LoginService;

@Route(value = "login")
@PageTitle("Login")
public class LoginView extends VerticalLayout {
    @Autowired
    Session session;

    @Autowired
    LoginService loginService;


    public LoginView() {
        Label header = new Label("Knowledge base");
        add(header);

        LoginForm component = new LoginForm();
        Button restoreLogin = new Button("Restore login button",
                event -> component.setEnabled(true));
        Button showError = new Button("Show error",
                event -> component.setError(true));
        component.setForgotPasswordButtonVisible(false);
        add(component);

        component.addLoginListener(loginEvent -> {
            if ( loginService.validateCredentials(loginEvent.getUsername(),loginEvent.getPassword())) {
                setAuth();
                this.getUI().ifPresent(ui -> ui.navigate("knowledge"));
            } else {
                showError.click();
                restoreLogin.click();
            }
        });
        setDefaultHorizontalComponentAlignment(Alignment.CENTER);
    }

    private void setAuth() {
        session.setAuth(true);
    }
}