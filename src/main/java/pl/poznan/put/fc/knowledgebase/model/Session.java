package pl.poznan.put.fc.knowledgebase.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Session {
    private boolean auth;
}
