package pl.poznan.put.fc.knowledgebase.view;

import com.microsoft.azure.storage.StorageException;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import org.vaadin.firitin.components.DynamicFileDownloader;
import pl.poznan.put.fc.knowledgebase.model.Document;
import pl.poznan.put.fc.knowledgebase.model.Session;
import pl.poznan.put.fc.knowledgebase.service.DocumentService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Route(value = "knowledge")
@PageTitle(value = "Knowledge base")
public class MainView extends VerticalLayout {
    private DocumentService documentService;
    private Grid<Document> documentGrid = new Grid<>(Document.class);
    private ComboBox<String> comboBox = new ComboBox<>();
    private TextField query = new TextField();
    private TextField tags = new TextField();
    private String temp = "";
    private Session session;

    public MainView(DocumentService documentService, Session session) {
        this.session = session;
        this.documentService = documentService;
        if (session.isAuth()) {
            initView();
        } else {
            UI.getCurrent().navigate("login");
            UI.getCurrent().getPage().reload();
        }
    }

    private void initView() {
        addHeader();
        addSearch();
        addDocumentsGrid();
    }

    private void addSearch() {
        HorizontalLayout tagsLayout = new HorizontalLayout();
        Button button = new Button();

        query = new TextField();
        query.setPlaceholder("Search...");
        query.setClearButtonVisible(true);
        query.setWidth("75%");
        query.addValueChangeListener(event -> {
               if(event.getValue().isEmpty()){
                   tags.setReadOnly(true);
                   temp = tags.getValue();
                   tags.setValue("");
                   button.setEnabled(false);
               }else if(event.getOldValue().isEmpty()){
                   tags.setReadOnly(false);
                   tags.setValue(temp);
                   button.setEnabled(true);

               }
        });
        query.setValueChangeMode(ValueChangeMode.EAGER);

        tags = new TextField();
        tags.setPlaceholder("Search...");
        tags.setClearButtonVisible(true);
        tags.setWidth("74%");
        tags.setReadOnly(true);

        Label tagsLabel = new Label("  Tag");
        tagsLabel.setWidth("20%");

        comboBox = new ComboBox<>();
        comboBox.setItems("Content by meaning", "Content by phrase", "Tag", "Name");
        comboBox.setValue("Content by meaning");
        comboBox.setWidth("20%");
        comboBox.addValueChangeListener(event -> {
            if ("Tag".equalsIgnoreCase(event.getValue())) {
                tags.setValue("");
                tagsLayout.setVisible(false);
            } else if ("Tag".equalsIgnoreCase(event.getOldValue())) {
                tagsLayout.setVisible(true);
            }
        });

        button.addClickListener(buttonClickEvent -> {
            search();
        });
        button.setEnabled(false);
        button.setIcon(VaadinIcon.SEARCH.create());

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        setHorizontalComponentAlignment(Alignment.CENTER, horizontalLayout);
        horizontalLayout.setWidth("80%");
        horizontalLayout.add(query);
        horizontalLayout.add(comboBox);
        horizontalLayout.add(button);

        setHorizontalComponentAlignment(Alignment.CENTER, tagsLayout);
        tagsLayout.setWidth("80%");
        tagsLayout.add(tags);
        tagsLayout.add(tagsLabel);
        tagsLayout.setVerticalComponentAlignment(Alignment.CENTER, tagsLabel);

        add(horizontalLayout);
        add(tagsLayout);
    }

    private void addHeader() {
        Label header = new Label("Knowledge base");
        Button logout = new Button();
        logout.addClickListener(buttonClickEvent -> {
            session.setAuth(false);
            UI.getCurrent().navigate("login");
            UI.getCurrent().getPage().reload();
        });
        logout.setText("Logout");
        add(logout);
        add(header);

        setHorizontalComponentAlignment(Alignment.END, logout);
        setHorizontalComponentAlignment(Alignment.CENTER, header);
    }

    private void addDocumentsGrid() {
        documentGrid.setWidth("80%");
        documentGrid.setColumns("name","rank");
        documentGrid.addColumn(
                new ComponentRenderer<>(document -> {
                    Button button = new Button();
                    button.setText("Show tags");
                    if (document.getTags().isEmpty()) {
                        button.setEnabled(false);
                    }
                    button.addClickListener(buttonClickEvent -> {
                        Dialog dialog = new Dialog();
                        dialog.setMaxWidth("1000px");
                        document.getTags().forEach(s -> {
                            HorizontalLayout layout = new HorizontalLayout();
                            layout.add(new Label(s));
                            dialog.add(layout);
                        });
                        dialog.open();
                    });
                    return button;
                })
        );

        documentGrid.addColumn(
                new ComponentRenderer<>(document -> {
                    Button button = new Button();
                    button.setText("Add tags");
                    button.addClickListener(buttonClickEvent -> {
                        Dialog dialog = new Dialog();
                        dialog.setMaxWidth("1000px");
                        TextField textField = new TextField();
                        dialog.add(textField);
                        Button addButton = new Button("Add");
                        addButton.addClickListener(addButtonClickEvent -> {
                            documentService.addTagsToDocument(document, Arrays.asList(textField.getValue().split(",")));
                            search();
                            dialog.close();
                        });
                        dialog.add(addButton);
                        dialog.open();
                    });
                    return button;
                })
        );

        documentGrid.addColumn(
                new ComponentRenderer<>(document -> {
                    Button button = new Button();
                    button.setText("Preview");
                    button.addClickListener(buttonClickEvent -> {
                        Dialog dialog = new Dialog();
                        dialog.setMaxWidth("1000px");
                        try {
                            com.vaadin.flow.component.Component content = null;
                            if (document.getExtension().equalsIgnoreCase(".pdf")) {
                                byte[] finalImageBytes = documentService.getPdfPreview(document);
                                StreamResource resource = new StreamResource("document.jpg", () -> new ByteArrayInputStream(finalImageBytes));
                                content = new Image(resource, document.getName());
                            } else {
                                content = new Label(document.getTextContent());
                            }
                            dialog.add(content);
                            dialog.open();
                        } catch (IOException | StorageException | InvalidKeyException | URISyntaxException e) {
                            // :(
                        }
                    });
                    return button;
                })
        );

        documentGrid.addColumn(
                new ComponentRenderer<>(document -> new DynamicFileDownloader("Download", document.getName(),
                        outputStream -> {
                            try {
                                outputStream.write(documentService.getDocumentFile(document));
                            } catch (IOException | URISyntaxException | StorageException | InvalidKeyException ignored) {
                                // :(
                            }
                        }))
        );
        setHorizontalComponentAlignment(Alignment.CENTER, documentGrid);
        add(documentGrid);
    }

    private void search() {
        List<Document> result = new ArrayList<>();
        if (!query.getValue().isBlank()) {
            if (comboBox.getValue() != null && comboBox.getValue().equalsIgnoreCase("Content by meaning")) {
                result = documentService.searchByContentMeaning(query.getValue());
            } else if (comboBox.getValue() != null && comboBox.getValue().equalsIgnoreCase("Tag")) {
                result = documentService.searchByTags(query.getValue());
            } else if (comboBox.getValue() != null && comboBox.getValue().equalsIgnoreCase("Content by phrase")) {
                result = documentService.searchByContentPhrase(query.getValue());
            } else if (comboBox.getValue() != null && comboBox.getValue().equalsIgnoreCase("Name")) {
                result = documentService.searchByName(query.getValue());
            } else {
                Notification.show("Wrong search type");
            }
            if (!tags.getValue().isEmpty()) {
                result.removeIf(document -> document.getTags().stream().noneMatch(tag -> tag.equalsIgnoreCase(tags.getValue())));
            }
        }
        documentGrid.setItems(result);
    }

}

