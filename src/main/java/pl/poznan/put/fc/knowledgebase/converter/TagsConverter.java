package pl.poznan.put.fc.knowledgebase.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Converter
public class TagsConverter implements AttributeConverter<List<String>, String> {

    @Override
    public String convertToDatabaseColumn(List<String> strings) {
        if (strings == null || strings.isEmpty()) {
            return "";
        }
        return strings.stream().collect(Collectors.joining(";"));
    }

    @Override
    public List<String> convertToEntityAttribute(String s) {
        if (s == null || s.isBlank()) {
            return new ArrayList<>();
        }
        List<String> list = new ArrayList<>();
        list.addAll(Arrays.asList(s.split(";")));
        return list;
    }
}
