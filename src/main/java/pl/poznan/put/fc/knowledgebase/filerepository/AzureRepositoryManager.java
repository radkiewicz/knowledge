package pl.poznan.put.fc.knowledgebase.filerepository;

import com.microsoft.azure.storage.StorageCredentials;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.StorageUri;
import com.microsoft.azure.storage.file.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.poznan.put.fc.knowledgebase.model.Document;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.List;

@Service
public class AzureRepositoryManager {
    @Autowired
    FileClientProvider fileClientProvider;

    public List<Document> getAllDocuments() throws IOException, URISyntaxException, InvalidKeyException, StorageException {
        CloudFileDirectory knowledge = fileClientProvider.getFileClientReference().getShareReference("knowledge").getRootDirectoryReference();
        List<CloudFile> files = new ArrayList<>();
        getAllFiles(knowledge, files);

        List<Document> result = new ArrayList<>();
        files.forEach(cloudFile -> result.add(Document.builder()
                .path(cloudFile.getStorageUri().getPrimaryUri().toString())
                .name(cloudFile.getName())
                .build()));
        return result;
    }

    public CloudFile getFile(String primaryUrl) throws InvalidKeyException, IOException, URISyntaxException, StorageException {
        StorageCredentials credentials = fileClientProvider.getFileClientReference().getCredentials();
        StorageUri storageUri = new StorageUri(new URI(primaryUrl));
        return new CloudFile(storageUri, credentials);
    }

    private void getAllFiles(CloudFileDirectory rootDir, List<CloudFile> result) throws StorageException {
        Iterable<ListFileItem> results = rootDir.listFilesAndDirectories();
        for (ListFileItem item : results) {
            if (item.getClass() == CloudFileDirectory.class) {
                getAllFiles((CloudFileDirectory) item, result);
            } else {
                String s = item.getStorageUri().getPrimaryUri().toString();
                String substring = s.substring(s.lastIndexOf("."));
                if (isFileInValidFormat(substring)) {
                    result.add((CloudFile) item);
                }
            }
        }
    }

    private boolean isFileInValidFormat(String file) {
        Path path = Paths.get(file);
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher("regex:^.*.+(pdf|docx|txt|doc)$");
        return matcher.matches(path);
    }
}
