package pl.poznan.put.fc.knowledgebase.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.poznan.put.fc.knowledgebase.model.User;

@Repository
public interface UsersRepository extends JpaRepository<User, Integer> {
    User findFirstByLogin(String login);
}
