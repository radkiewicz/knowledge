package pl.poznan.put.fc.knowledgebase.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.annotations.Generated;
import pl.poznan.put.fc.knowledgebase.converter.TagsConverter;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@Entity(name = "document")
@NoArgsConstructor
@AllArgsConstructor
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String path;
    @JsonIgnore
    private String textContent;
    @Convert(converter = TagsConverter.class)
    private List<String> tags;
    private String name;
    @Transient
    private Integer rank;

    public String getExtension() {
        return name.substring(name.lastIndexOf("."));
    }
}
