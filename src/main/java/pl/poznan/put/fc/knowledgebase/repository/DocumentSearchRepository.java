package pl.poznan.put.fc.knowledgebase.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.poznan.put.fc.knowledgebase.model.Document;
import pl.poznan.put.fc.knowledgebase.model.RankedDocument;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class DocumentSearchRepository {

    @Autowired
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    public List<Document> freeTextTableSearch(String query) {
        List<RankedDocument> resultList = entityManager.createNativeQuery(String.format("select Id, Name, Path, Text_content, Tags, Rank from document as doc inner join FREETEXTTABLE (document , Text_content, '%s',  LANGUAGE 1045) as free on doc.Id = free.[key] ORDER BY RANK DESC", query), RankedDocument.class).getResultList();
        return resultList.stream()
                .map(rd-> Document.builder()
                        .name(rd.getName())
                        .path(rd.getPath())
                        .id(rd.getId())
                        .rank(rd.getRank())
                        .tags(rd.getTags())
                        .textContent(rd.getTextContent())
                        .build()).collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    public List<Document> containsSearch(String query) {
        if (query.split(" ").length > 1) {
            String replace = query.replace(" ", ",");
            return entityManager.createNativeQuery(String.format("Select * from document WHERE CONTAINS(Text_content, 'near((%s),1,true)', LANGUAGE 1045)", replace), Document.class).getResultList();
        } else {
            return entityManager.createNativeQuery(String.format("Select * from document WHERE CONTAINS(Text_content, '%s', LANGUAGE 1045)", query), Document.class).getResultList();
        }
    }
}
