package pl.poznan.put.fc.knowledgebase.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.poznan.put.fc.knowledgebase.converter.TagsConverter;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "test")
public class RankedDocument {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String path;
    @JsonIgnore
    private String textContent;
    @Convert(converter = TagsConverter.class)
    private List<String> tags;
    private String name;
    private Integer rank;
}
